<?php
/**
 *
 */
class Administrator extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->library('encrypt');
    $this->load->model('all-login/sign');
    $this->load->model('admin-master/admin_master');
    $this->load->model('admin-department/admin_department');
  }

  function formLogin(){
    if ($this->session->userdata('USER_LEVEL_ANNOUNCEMENT') == 'MASTER') {
      //echo "MASTER";
      redirect(base_url('masterdashboard'));
    } else if ($this->session->userdata('USER_LEVEL_ANNOUNCEMENT') == 'DEPARTMENT') {
      echo "DEPARTMENT";
    } else {
      $db['title'] = "Administrator | Announcement Broadcaster";
      $this->load->view('login-folder/admin-master-login',$db);
    }
  }
  function actLoginAdmin(){
    $this->sign->actLoginAdmin();
  }
  function actLogoutAdmin(){
    $this->session->unset_userdata('USER_ID_ANNOUNCEMENT');
    $this->session->unset_userdata('USER_NAME_ANNOUNCEMENT');
    $this->session->unset_userdata('USER_LEVEL_ANNOUNCEMENT');
    $this->session->unset_userdata('USER_DEPARTMENT_ANNOUNCEMENT');
    session_destroy();
    redirect(base_url());
  }

  //===================================================================================//

  function masterDashboard(){
    if ($this->session->userdata('USER_LEVEL_ANNOUNCEMENT') == "MASTER") {
      $db['title_master'] = "Home | Master Administrator | Announcement Broadcaster";
      $this->load->view('template-master-admin/header',$db);
      $this->load->view('master-admin-folder/index');
    }else if ($this->session->userdata('USER_LEVEL_ANNOUNCEMENT') == "DEPARTMENT"){
      $db['title_master'] = "Home | Department Administrator | Announcement Broadcaster";
      $this->load->view('template-department-admin/header',$db);
      $this->load->view('department-admin-folder/index');
    }
  }
  function showDepartment(){
    $db['title_master'] = "Data Department | Master Administrator | Announcement Broadcaster";
    $db['showDepartment'] = $this->admin_master->showDepartment();
    $this->load->view('template-master-admin/header',$db);
    $this->load->view('master-admin-folder/data-department');
  }
  function showCollegeStudent(){
    $db['title_master'] = "Data College Student | Master Administrator | Announcement Broadcaster";
    $this->load->view('template-master-admin/header',$db);
    $this->load->view('master-admin-folder/data-college-student');
  }
  function showUserDepartment(){
    $db['title_master'] = "Data User Department | Master Administrator | Announcement Broadcaster";
    $this->load->view('template-master-admin/header',$db);
    $this->load->view('master-admin-folder/data-user-department');
  }
  function showUserMaster(){
    $db['title_master'] = "Data User Master | Master Administrator | Announcement Broadcaster";
    $this->load->view('template-master-admin/header',$db);
    $this->load->view('master-admin-folder/data-user-master');
  }


//========================================================================================
  // CREATE ANNOUNCEMENT

  // FOR ADMIN MASTER
  function actPostAnnouncementMaster(){
    $this->admin_master->actPostAnnouncementMaster();
  }
  // FOR ADMIN DEPARTMENT
  function actPostAnnouncementDepartment(){
    $this->admin_department->actPostAnnouncementDepartment();
  }

//========================================================================================

//========================================================================================
  //CHOOSING DEPARTMENT

  //FOR ADMIN MASTER
  function formPostListReceiverAnnouncementMaster($slug, $id){
    $replacing_url            = str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
    $id_announcement          = $this->encrypt->decode($replacing_url);
    $db['title_master']       = "Choosing List | Master Administrator | Announcement Broadcaster";
    $db['idAnnouncementForUpdate'] = $id_announcement;
    $db['showDepartment']     = $this->admin_master->showDepartment();
    $db['showListReceiverAnnouncementMaster'] = $this->admin_master->showListReceiverAnnouncementMaster($id_announcement);
    $this->load->view('template-master-admin/header',$db);
    $this->load->view('master-admin-folder/choosing-list',$db);
  }
  function showListReceiverMaster($id){
    $db['showListReceiverAnnouncementMaster'] = $this->admin_master->showListReceiverAnnouncementMaster($id);
    $this->load->view('master-admin-folder/choosing-list-table',$db);
  }

  //FOR ADMIN DEPARTMENT
  function formPostListReceiverAnnouncementDepartment($slug, $id){
    $replacing_url            = str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
    $id_announcement          = $this->encrypt->decode($replacing_url);
    $db['title_master']       = "Choosing List | Department Administrator | Announcement Broadcaster";
    $db['idAnnouncementForUpdate'] = $id_announcement;
    $db['showDepartment']     = $this->admin_department->showDepartment();
    $db['showListReceiverAnnouncementDepartment'] = $this->admin_department->showListReceiverAnnouncementDepartment($id_announcement);
    $this->load->view('template-department-admin/header',$db);
    $this->load->view('department-admin-folder/choosing-list',$db);
  }
  function showListReceiver($id){
    $db['showListReceiverAnnouncementDepartment'] = $this->admin_department->showListReceiverAnnouncementDepartment($id);
    $this->load->view('department-admin-folder/choosing-list-table',$db);
  }
//========================================================================================

//========================================================================================
  //CREATE LIST DEPARTMENT
  //FOR ADMIN MASTER
  function actPostListReceiverAnnouncementMaster($department){
    $this->admin_master->actPostListReceiverAnnouncementMaster($department);
  }
  //FOR ADMIN DEPARTMENT
  function actPostListReceiverAnnouncementDepartmentAllinOne(){
    $this->admin_department->actPostListReceiverAnnouncementDepartmentAllinOne();
  }
  function actPostListReceiverAnnouncementDepartmentOneSemesterAllClass($semester){
    $this->admin_department->actPostListReceiverAnnouncementDepartmentOneSemesterAllClass($semester);
  }
  function actPostListReceiverAnnouncementDepartmentOneSemesterOneClass($semester, $class){
    $this->admin_department->actPostListReceiverAnnouncementDepartmentOneSemesterOneClass($semester, $class);
  }
//========================================================================================

}
 ?>
