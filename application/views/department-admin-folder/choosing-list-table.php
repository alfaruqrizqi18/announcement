<table id="table3" class="table table-hover table-fw-widget">
  <thead>
    <tr>
      <th>Announcement Title</th>
      <th>Department</th>
      <th>Semester</th>
      <th>Class</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($showListReceiverAnnouncementDepartment as $data) {?>
      <tr class="gradeA">
        <td><?php echo $data['title'] ?></td>
        <td><?php echo $data['name_department'] ?></td>
        <td><?php echo $data['semester'] ?></td>
        <td><?php echo $data['class'] ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>

  <script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
  <script src="assets/js/app-tables-datatables.js" type="text/javascript"></script>


  <script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    App.init();
    App.dataTables();
  });
  </script>
