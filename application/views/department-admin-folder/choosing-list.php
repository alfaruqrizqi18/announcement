
<div class="be-content">
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default panel-border-color panel-border-color-primary">
          <div class="panel-heading panel-heading-divider">List Chooser<span class="panel-subtitle">Choose the Department and send the Announcement</span></div>
          <div class="panel-body">
            <form id="actChoosingList" class="form-horizontal">
              <div class="form-group">
                <input type="text" name="idAnnouncementForUpdate" id="idAnnouncementForUpdate" value="<?php echo $idAnnouncementForUpdate; ?>">
                <label class="col-sm-3 control-label">Department</label>
                <div class="col-sm-6">
                  <select class="select2" name="department" id="department">
                    <option value="<?php echo $this->session->userdata('USER_DEPARTMENT_ID_ANNOUNCEMENT') ?>"><?php echo $this->session->userdata('USER_DEPARTMENT_ANNOUNCEMENT'); ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Semester</label>
                <div class="col-sm-3">
                  <select class="select2" id="semester" name="semester">
                    <optgroup label="Special">
                      <option value="0">Send to All Semester</option>
                    </optgroup>
                    <optgroup label="Specific">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                    </optgroup>
                  </select>
                </div>
                <div class="col-md-1">
                  <label class="col-md-1 control-label">Class</label>
                </div>
                <div class="col-sm-2" id="kelas-body">
                  <select class="select2" name="kelas" id="kelas">
                    <optgroup label="Special">
                      <option value="0">Send to All Class</option>
                    </optgroup>
                    <optgroup label="Specific">
                      <option value="A">A</option>
                      <option value="B">B</option>
                      <option value="C">C</option>
                      <option value="D">D</option>
                      <option value="E">E</option>
                      <option value="F">F</option>
                    </optgroup>
                  </select>
                </div>
              </div>
              <div class="row xs-pt-15">
                <div class="col-md-12">
                  <p class="text-center">
                    <button type="submit" class="btn btn-space btn-primary btn-lg">
                      <i class="icon icon-left mdi mdi-check-all"></i>
                      Send Announcement</button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div class="panel panel-default panel-table">
              <div class="panel-body" id="table-panel-body">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var semester       = "";
var kelas          = "";
var idAnnouncement = $('#idAnnouncementForUpdate').val();
var department     = $('#department').val();
if ($('#semester').val() == 0) {
  $('#kelas-body').hide();
}
$('#semester').change(function(){
  semester = $(this).val();
  if (semester > 0) {
    $('#kelas-body').show();
  }else{
    $('#kelas-body').hide();
  }
})
$('#kelas').change(function(){
  kelas = $(this).val();
})
$('#table-panel-body').load('administrator/showListReceiver/'+idAnnouncement);

$(document).ready(function(){
  var formdata       = $("#actChoosingList").serialize();;
  $("#actChoosingList").submit(function(){
    if ($('#semester').val() == 0) {
      //ALL IN ONE DEPARTMENT
      //alert('All in One');
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('actChoosingListDepartmentAllinOne') ?>",
        data:  formdata,
        cache: false,
        success: function(result){
          $('#table-panel-body').load('administrator/showListReceiver/'+idAnnouncement);
          //alert(result);
        },
        error: function(result){
          alert('gagal');
        }
      });
    }else if ($('#semester').val() > 0 && $('#kelas').val() == 0){
      //ONE SEMESTER & ALL CLASS
      $.ajax({
        type: "POST",
        url: "actChoosingListDepartmentOneSemesterAllClass/"+semester,
        data: formdata,
        cache: false,
        success: function(result){
          $('#table-panel-body').load('administrator/showListReceiver/'+idAnnouncement);
          //alert(result);
        },
        error: function(result){
          alert('gagal');
        }
      });
      // alert($('#semester').val());
      // alert('One Semester & All Class');
    }else if($('#semester').val() > 0 && $('#kelas').val() != 0 ){
      //ONE SEMESTER & ONE CLASS
      $.ajax({
        type: "POST",
        url: "actChoosingListDepartmentOneSemesterOneClass/"+semester+"/"+kelas,
        data:  formdata,
        cache: false,
        success: function(result){
          $('#table-panel-body').load('administrator/showListReceiver/'+idAnnouncement);
          alert(result);
        },
        error: function(result){
          alert('gagal');
        }
      });
      // /alert('One Semester & One Class');
    }
    return false;
  });
});
</script>
<script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>
<script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/js/app-form-elements.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();
  App.formElements();
});
</script>
</body>
</html>
