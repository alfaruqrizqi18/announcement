<!DOCTYPE html>
<html lang="en">
<base href="<?php echo base_url(); ?>" target="">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title><?php echo $title_master; ?></title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/bootstrap-slider/css/bootstrap-slider.css"/>
    <link rel="stylesheet" href="assets/css/style.min.css" type="text/css"/>
    <script src="assets/js/jquery.min.js"></script>
  </head>
  <body>
    <div class="be-wrapper be-nosidebar-left">
      <nav class="navbar navbar-default navbar-fixed-top be-top-header">
        <div class="container-fluid">
          <div class="navbar-header"><a href="masterDashboard" class="navbar-brand"></a></div>
          <div class="be-right-navbar">
            <ul class="nav navbar-nav navbar-right be-user-nav">
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="assets/img/avatar.png" alt="Avatar"><span class="user-name">Túpac Amaru</span></a>
                <ul role="menu" class="dropdown-menu">
                  <li>
                    <div class="user-info">
                      <div class="user-name"><?php echo $this->session->userdata('USER_NAME_ANNOUNCEMENT'); ?></div>
                      <div class="user-position online">Available</div>
                    </div>
                  </li>
                  <li><a href="actLogoutAdmin"><span class="icon mdi mdi-power"></span> Sign out</a></li>
                </ul>
              </li>
            </ul>
          </div><a href="#" data-toggle="collapse" data-target="#be-navbar-collapse" class="be-toggle-top-header-menu collapsed">Wihout Sidebars</a>
          <div id="be-navbar-collapse" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="masterDashboard">Home</a></li>
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">Master Data <span class="mdi mdi-caret-down"></span></a>
                <ul role="menu" class="dropdown-menu">
                  <li><a href="administrator/showDepartment">Department</a></li>
                  <li><a href="administrator/showCollegeStudent">College Student</a></li>
                  <li><a href="administrator/showUserDepartment">Departement - Administrator</a></li>
                  <li><a href="administrator/showUserMaster">Master - Administrator</a></li>
                  <li><a href="administrator/showDataAnnouncement">Data Announcement</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
