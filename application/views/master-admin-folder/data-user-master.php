<div class="be-content">
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default panel-border-color panel-border-color-primary">
          <div class="panel-heading panel-heading-divider">Data User Master<span class="panel-subtitle">Data Master Admin</span></div>
          <div class="panel-body">
          </div>
          <div class="panel panel-default panel-table">
            <div class="panel-body" id="table-panel-body">
              <table id="table3" class="table table-hover table-fw-widget">
                <thead>
                  <tr>
                    <th>#ID Administrator</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Department</th>
                    <th>Level</th>
                  </tr>
                </thead>
                <tbody>
                  <?php //foreach ($showListReceiverAnnouncementMaster as $data) {?>
                    <tr class="gradeA">
                      <td><?php //echo $data['title'] ?></td>
                      <td><?php //echo $data['name_department'] ?></td>
                    </tr>
                    <?php //} ?>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
</script>
<script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>
<script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/js/app-form-elements.js" type="text/javascript"></script>

<script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
<script src="assets/js/app-tables-datatables.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();
  App.dataTables();
  App.formElements();
});
</script>
</body>
</html>
