<div class="be-content">
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default panel-border-color panel-border-color-primary">
          <div class="panel-heading panel-heading-divider">List Chooser<span class="panel-subtitle">Choose the Department and send the Announcement</span></div>
          <div class="panel-body">
            <form id="actChoosingListMaster" class="form-horizontal">
              <div class="form-group">
                <input type="text" name="idAnnouncementForUpdate" id="idAnnouncementForUpdate" value="<?php echo $idAnnouncementForUpdate; ?>">
                <label class="col-sm-3 control-label">Department</label>
                <div class="col-sm-6">
                  <select class="select2" name="department" id="department">
                    <optgroup label="Special">
                      <option value="0">Send to All Department</option>
                    </optgroup>
                    <optgroup label="Specific Department">
                      <?php foreach ($showDepartment as $data) {
                        $department = "Only for Master Admin";
                        if ($data['name']!= $department) {?>
                          <option value="<?php echo $data['id_department'] ?>"><?php echo $data['name'] ?></option>
                          <?php } ?>
                          <?php } ?>
                        </optgroup>
                      </select>
                    </div>
                  </div>
                  <div class="row xs-pt-15">
                    <div class="col-md-12">
                      <p class="text-center">
                        <button type="submit" class="btn btn-space btn-primary btn-lg">
                          <i class="icon icon-left mdi mdi-check-all"></i>
                          Send Announcement</button>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="panel panel-default panel-table">
                  <div class="panel-body" id="table-panel-body">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    var idAnnouncement = $('#idAnnouncementForUpdate').val();
    var department     = "0";
    $('#department').change(function(){
      department = $(this).val();
    })
    $('#table-panel-body').load('administrator/showListReceiverMaster/'+idAnnouncement);
    $(document).ready(function(){
      var formdata       = $("#actChoosingListMaster").serialize();;
      $("#actChoosingListMaster").submit(function(){
          //ALL IN ONE DEPARTMENT
          //alert('All in One');
          $.ajax({
            type: "POST",
            url: "actChoosingListMaster/"+department,
            data:  formdata,
            cache: false,
            success: function(result){
              $('#table-panel-body').load('administrator/showListReceiverMaster/'+idAnnouncement);
            },
            error: function(result){
              alert('gagal');
            }
          });
        return false;
      });
    });
    </script>
    <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/main.js" type="text/javascript"></script>
    <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="assets/js/app-form-elements.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
      App.formElements();
    });
    </script>
  </body>
  </html>
