      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Create new Announcement<span class="panel-subtitle">By filling these form-fill you will create an announcement</span></div>
                <div class="panel-body">
                  <form action="actNewPostAnnouncementMaster" method="post">
                    <div class="form-group xs-pt-10">
                      <label>Announcement Title</label>
                      <input type="text" placeholder="Enter an Announcement TItle here..." class="form-control input-lg" name="tf_title" autocomplete="off" autofocus="" required="">
                    </div>
                    <div class="form-group">
                      <label>Announcement Content</label>
                      <textarea rows="6" cols="80" placeholder="Enter an Announcement Content here..." class="form-control" name="tf_content" required=""></textarea>
                    </div>

                    <div class="row xs-pt-15">
                      <div class="col-md-12">
                        <p class="text-center">
                          <button type="submit" class="btn btn-space btn-primary btn-lg">
                            <i class="icon icon-left mdi mdi-cloud-done"></i>
                            Create this Announcement</button>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/main.js" type="text/javascript"></script>
    <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/lib/prettify/prettify.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();

      	//Runs prettify
      	prettyPrint();
      });
    </script>
  </body>
</html>
