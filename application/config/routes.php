<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'administrator/formLogin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//ALL ADMINISTRATOR DASHBOARD
$route['masterDashboard'] = 'administrator/masterDashboard';
//ALL ADMINISTRATOR DASHBOARD

//ADMINISTRATOR MASTER
$route['actLoginAdmin'] = 'administrator/actLoginAdmin';
$route['actLogoutAdmin'] = 'administrator/actLogoutAdmin';
$route['actNewPostAnnouncementMaster'] = 'administrator/actPostAnnouncementMaster';
$route['formChoosingListMaster/(:any)/(:any)'] = 'administrator/formPostListReceiverAnnouncementMaster/$1/$2';
$route['actChoosingListMaster/(:any)'] = 'administrator/actPostListReceiverAnnouncementMaster/$1';
//ADMINISTRATOR MASTER

//ADMINISTRATOR DEPARTMENT
$route['actNewPostAnnouncementDepartment'] = 'administrator/actPostAnnouncementDepartment';
$route['formChoosingListDepartment/(:any)/(:any)'] = 'administrator/formPostListReceiverAnnouncementDepartment/$1/$2';
$route['actChoosingListDepartmentAllinOne'] = 'administrator/actPostListReceiverAnnouncementDepartmentAllinOne';
$route['actChoosingListDepartmentOneSemesterAllClass/(:any)'] = 'administrator/actPostListReceiverAnnouncementDepartmentOneSemesterAllClass/$1';
$route['actChoosingListDepartmentOneSemesterOneClass/(:any)/(:any)'] = 'administrator/actPostListReceiverAnnouncementDepartmentOneSemesterOneClass/$1/$2';
//ADMINISTRATOR DEPARTMENT
