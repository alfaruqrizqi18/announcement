<?php
/**
 *
 */
class Admin_master extends CI_Model{

  function __construct(){
    parent::__construct();
    $this->load->database();
    $this->load->library('encrypt');
  }

  function actPostAnnouncementMaster(){
    $title      = $_POST['tf_title'];
    $slug_title = url_title($title, 'dash', TRUE);
    $content    = $_POST['tf_content'];
    $session_id = $this->session->userdata('USER_ID_ANNOUNCEMENT');
    $type       = "Master";
    $data       = array(
                  'title'             => $title,
                  'slug_title'        => $slug_title,
                  'content'           => $content,
                  'announcement_type' => $type,
                  'id_admin'          => $session_id
                  );
    $this->db->insert('announcement_master', $data);
    $query      = $this->db->query(
                  "SELECT *
                   FROM announcement_master
                   WHERE title = '$title'
                   AND slug_title = '$slug_title'
                   AND content = '$content'
                   AND announcement_type = '$type'
                   AND id_admin = '$session_id'
                  ");
    foreach ($query->result() as $data) {
      $id_announcement         = $this->encrypt->encode($data->id_announcement);
      $final_id_announcement   = str_replace(array('+', '/', '='), array('-', '_', '~'), $id_announcement);
      $slug_title_by_id        = $data->slug_title;
    }
    redirect(base_url('formChoosingListMaster/'.$slug_title_by_id."/".$final_id_announcement));
  }
  function showListReceiverAnnouncementMaster($id){
    $query      = $this->db->query(
                  "SELECT list_department.id_department, list_department.id_announcement,
                          announcement_master.title, department.name as name_department,
                          semester, class
                   FROM list_department
                   LEFT JOIN department ON department.id_department = list_department.id_department
                   LEFT JOIN announcement_master ON announcement_master.id_announcement = list_department.id_announcement
                   WHERE list_department.id_announcement = '$id'
                  ");
    return $query->result_array();
  }
  function showDepartment(){
    $query      = $this->db->query(
                  "SELECT *
                   FROM department
                  ");
    return $query->result_array();
  }
  function actPostListReceiverAnnouncementMaster($department){
    $idAnnouncement = $_POST['idAnnouncementForUpdate'];
    if ($department == "0") {
      $query_department    = $this->db->query(
                             "SELECT *
                              FROM department
                              WHERE id_department != 1
                             ");
      foreach ($query_department->result() as $data_department) {
        $id_department                = $data_department->id_department;
        $data_post_to_list_department = array(
                                        'id_announcement' => $idAnnouncement,
                                        'id_department'   => $id_department
                                        );
        $this->db->insert('list_department', $data_post_to_list_department);
        $query_get_latest_data_list_department = $this->db->query(
                                                 "SELECT *
                                                  FROM list_department
                                                  WHERE id_announcement = '$idAnnouncement'
                                                  AND id_department = '$id_department'
                                                  ");
        foreach ($query_get_latest_data_list_department->result() as $latest_data) {
          $id_list_department    = $latest_data->id_list_department;
          $query_college_student = $this->db->query(
                                   "SELECT college_student.id_college, college_student.name as student_name, college_student.semester,
                                           college_student.class, department.name as department_name
                                    FROM college_student
                                    LEFT JOIN department ON department.id_department = college_student.id_department
                                    WHERE college_student.id_department = '$id_department'
                                   ");
          foreach ($query_college_student->result() as $data_college_student) {
            $sender   = $this->session->userdata('USER_ID_ANNOUNCEMENT');
            $receiver = $data_college_student->id_college;
            $data_to_post_receiver_announcemet = array(
                                                 'id_announcement' => $idAnnouncement,
                                                 'id_list_department' => $id_list_department,
                                                 'sender'          => $sender,
                                                 'receiver'        => $receiver
                                                 );
            $this->db->insert('receiver_announcement', $data_to_post_receiver_announcemet);
          }
        }
      }
    }else{
      $data_post_to_list_department = array(
                                      'id_announcement' => $idAnnouncement, //Postnya diatas
                                      'id_department'   => $department //Postnya diatas
                                      );
      $this->db->insert('list_department', $data_post_to_list_department);
      $query_get_latest_data_list_department = $this->db->query(
                                               "SELECT *
                                                FROM list_department
                                                WHERE id_announcement = '$idAnnouncement'
                                                AND id_department = '$department'
                                                ");
      foreach ($query_get_latest_data_list_department->result() as $latest_data) {
        $id_list_department = $latest_data->id_list_department;
        $query_college_student = $this->db->query(
                               "SELECT college_student.id_college, college_student.name as student_name, college_student.semester,
                                       college_student.class, department.name as department_name
                                FROM college_student
                                LEFT JOIN department ON department.id_department = college_student.id_department
                                WHERE college_student.id_department = '$department'
                               ");
        foreach ($query_college_student->result() as $data_college_student) {
          $sender   = $this->session->userdata('USER_ID_ANNOUNCEMENT');
          $receiver = $data_college_student->id_college;
          $data_to_post_receiver_announcemet = array(
                                               'id_announcement'    => $idAnnouncement,
                                               'id_list_department' => $id_list_department,
                                               'sender'             => $sender,
                                               'receiver'           => $receiver
                                               );
          $this->db->insert('receiver_announcement', $data_to_post_receiver_announcemet);
        }
      }
    }
  }

}
 ?>
