-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 03 Mar 2017 pada 01.33
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `announcement_broadcaster`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_master`
--

CREATE TABLE `admin_master` (
  `id_admin` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `id_department` int(11) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_master`
--

INSERT INTO `admin_master` (`id_admin`, `name`, `username`, `password`, `id_department`, `level`) VALUES
(1, 'Master Admin', 'master', '123', 1, 'MASTER'),
(2, 'Master 2', 'master2', '123', 1, 'MASTER'),
(3, 'Master 3', 'master3', '123', 1, 'MASTER'),
(4, 'Master 4', 'master4', '123', 1, 'MASTER'),
(5, 'Department Admin 1', 'dprt1', '123', 2, 'DEPARTMENT'),
(6, 'Department 2', 'dprt2', '123', 3, 'DEPARTMENT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `announcement_master`
--

CREATE TABLE `announcement_master` (
  `id_announcement` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `slug_title` text NOT NULL,
  `content` text NOT NULL,
  `announcement_type` varchar(30) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_admin` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `announcement_master`
--

INSERT INTO `announcement_master` (`id_announcement`, `title`, `slug_title`, `content`, `announcement_type`, `created_on`, `id_admin`) VALUES
(1, 'Testing satu semester semua kelas', 'testing-satu-semester-semua-kelas', 'asdasdasd', 'Department', '2017-02-14 12:31:17', 6),
(2, 'Testing master', 'testing-master', 'asdasdasd', 'Master', '2017-02-14 13:45:05', 1),
(3, 'asdasd', 'asdasd', 'asdasd', 'Master', '2017-02-14 13:51:30', 1),
(4, 'Ayo segera kumpulkan proyekmu', 'ayo-segera-kumpulkan-proyekmu', 'Cepetan setan!', 'Master', '2017-02-14 15:20:56', 1),
(5, 'Pengumpulan Proyek 1', 'pengumpulan-proyek-1', 'Untuk proyek 1segera dikumpulkan paling lambat minggu depan\r\nProyek 1 bisa dikumpulkan ke mas ari.\r\n\r\n\r\nBest regard,\r\nYour admin', 'Department', '2017-02-14 15:22:47', 5),
(6, 'asdasd', 'asdasd', 'asdasdsad', 'Department', '2017-02-14 15:36:24', 6),
(7, 'Testing Apps', 'testing-apps', 'asdasd\r\nasd\r\nasd\r\nasd', 'Master', '2017-02-15 03:18:09', 1),
(8, 'Test', 'test', 'adasdasd', 'Department', '2017-02-28 01:54:59', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `college_student`
--

CREATE TABLE `college_student` (
  `id_college` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `id_department` int(3) NOT NULL,
  `semester` int(3) NOT NULL,
  `class` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `college_student`
--

INSERT INTO `college_student` (`id_college`, `name`, `username`, `password`, `id_department`, `semester`, `class`) VALUES
(1, 'Rizqi Alfaruq', '15010086', '123', 2, 3, 'B'),
(2, 'Rizqi Alfaruq 2', '15010087', '123', 2, 2, 'A'),
(3, 'Sumanti', '15010088', '123', 3, 3, 'A'),
(4, 'Sumanti 2', '15010089', '123', 3, 3, 'A'),
(5, 'Suprapto', '15010090', '123', 3, 2, 'A'),
(6, 'Suprapto 2', '15010091', '123', 3, 4, 'B'),
(7, 'Samijo', '15010092', '123', 3, 4, 'B'),
(8, 'Rizqi Alfaruq 3', '15010093', '123', 3, 4, 'C');

-- --------------------------------------------------------

--
-- Struktur dari tabel `department`
--

CREATE TABLE `department` (
  `id_department` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `department`
--

INSERT INTO `department` (`id_department`, `name`) VALUES
(1, 'Only for Master Admin'),
(2, 'Informations of Technic'),
(3, 'Accountant'),
(4, 'Maintenance and Machine Repairs'),
(5, 'Civil Engineering');

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_department`
--

CREATE TABLE `list_department` (
  `id_list_department` int(11) NOT NULL,
  `id_announcement` int(11) NOT NULL,
  `id_department` int(11) NOT NULL,
  `semester` int(11) DEFAULT NULL,
  `class` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `list_department`
--

INSERT INTO `list_department` (`id_list_department`, `id_announcement`, `id_department`, `semester`, `class`) VALUES
(1, 8, 2, NULL, NULL),
(2, 8, 2, 1, 'A'),
(3, 8, 2, 6, 'C');

-- --------------------------------------------------------

--
-- Struktur dari tabel `receiver_announcement`
--

CREATE TABLE `receiver_announcement` (
  `id_receiver_announcement` int(11) NOT NULL,
  `id_announcement` int(11) NOT NULL,
  `id_list_department` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `receiver_announcement`
--

INSERT INTO `receiver_announcement` (`id_receiver_announcement`, `id_announcement`, `id_list_department`, `sender`, `receiver`) VALUES
(29, 6, 22, 6, 3),
(30, 6, 22, 6, 4),
(31, 6, 22, 6, 5),
(32, 6, 22, 6, 6),
(33, 6, 22, 6, 7),
(34, 6, 22, 6, 8),
(35, 6, 23, 6, 5),
(36, 6, 24, 6, 3),
(37, 6, 24, 6, 4),
(38, 6, 25, 6, 6),
(39, 6, 25, 6, 7),
(40, 6, 26, 6, 8),
(41, 6, 27, 6, 3),
(42, 6, 27, 6, 4),
(43, 6, 27, 6, 5),
(44, 6, 27, 6, 6),
(45, 6, 27, 6, 7),
(46, 6, 27, 6, 8),
(47, 7, 28, 1, 3),
(48, 7, 28, 1, 4),
(49, 7, 28, 1, 5),
(50, 7, 28, 1, 6),
(51, 7, 28, 1, 7),
(52, 7, 28, 1, 8),
(53, 7, 29, 1, 1),
(54, 8, 1, 5, 1),
(55, 8, 1, 5, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_master`
--
ALTER TABLE `admin_master`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `id` (`id_admin`);

--
-- Indexes for table `announcement_master`
--
ALTER TABLE `announcement_master`
  ADD PRIMARY KEY (`id_announcement`);

--
-- Indexes for table `college_student`
--
ALTER TABLE `college_student`
  ADD PRIMARY KEY (`id_college`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id_department`);

--
-- Indexes for table `list_department`
--
ALTER TABLE `list_department`
  ADD PRIMARY KEY (`id_list_department`);

--
-- Indexes for table `receiver_announcement`
--
ALTER TABLE `receiver_announcement`
  ADD PRIMARY KEY (`id_receiver_announcement`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_master`
--
ALTER TABLE `admin_master`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `announcement_master`
--
ALTER TABLE `announcement_master`
  MODIFY `id_announcement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `college_student`
--
ALTER TABLE `college_student`
  MODIFY `id_college` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id_department` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `list_department`
--
ALTER TABLE `list_department`
  MODIFY `id_list_department` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `receiver_announcement`
--
ALTER TABLE `receiver_announcement`
  MODIFY `id_receiver_announcement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
